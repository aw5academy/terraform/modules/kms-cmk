data "aws_caller_identity" "current" {}

data "template_file" "cmk" {
  template = "${file("${path.module}/templates/cmk-policy.json.tpl")}"
  vars = {
    aws_account_id       = data.aws_caller_identity.current.account_id
    delivery-logs-access = var.delivery-logs-access
    logs-access          = var.logs-access
    region               = var.region
  }
}

resource "aws_kms_key" "cmk" {
  description         = "The encryption key for ${var.cmk-name}."
  enable_key_rotation = "true"
  policy              = data.template_file.cmk.rendered
  tags = {
    Name = var.cmk-name
  }
}

resource "aws_kms_alias" "cmk" {
  name          = "alias/${var.cmk-name}"
  target_key_id = aws_kms_key.cmk.key_id
}
