variable "cmk-name" {
  description = "The CMK name."
}

variable "delivery-logs-access" {
  default = "false"
  description = "A boolean controlling whether or not delivery.logs.amazonaws.com has cryptographic access to the CMK."
}

variable "logs-access" {
  default = "false"
  description = "A boolean controlling whether or not CloudWatch logs has cryptographic access to the CMK."
}

variable "region" {
  description = "The AWS region."
}
