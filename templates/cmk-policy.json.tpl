{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"Enable IAM User Permissions",
      "Effect":"Allow",
      "Principal":{
        "AWS":[
          "arn:aws:iam::${aws_account_id}:root"
        ]
      },
      "Action":"kms:*",
      "Resource":"*"
    }
    %{ if logs-access == "true" ~},
    {
      "Effect": "Allow",
      "Principal": { "Service": "logs.${region}.amazonaws.com" },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*"
    }
    %{ endif ~}
    %{ if delivery-logs-access == "true" ~},
    {
      "Effect": "Allow",
      "Principal": { "Service": "delivery.logs.amazonaws.com" },
      "Action": [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ],
      "Resource": "*"
    }
    %{ endif ~}
  ]
}
