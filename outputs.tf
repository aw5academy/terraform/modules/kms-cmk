output "alias-arn" {
  value = aws_kms_alias.cmk.arn
}

output "alias-name" {
  value = aws_kms_alias.cmk.name
}

output "arn" {
  value = aws_kms_key.cmk.arn
}

output "id" {
  value = aws_kms_key.cmk.key_id
}
