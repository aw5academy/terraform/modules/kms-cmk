# kms-cmk
A terraform module to create a [KMS](https://aws.amazon.com/kms/) CMK.

# Usage
The module can be used with:
```
module "kms-cmk" {
  cmk-name = "my-cmk"
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/kms-cmk.git"
  region   = "us-east-1"
}
```
